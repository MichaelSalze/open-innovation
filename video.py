import cv2
import numpy as np
import pickle
import datetime
import sqlite3

filepath = "./DataBase.db"
open(filepath, 'w').close() #crée un fichier vide
CreateDataBase = sqlite3.connect(filepath)
QueryCurs = CreateDataBase.cursor()
def CreateTable(nom_bdd):
    QueryCurs.execute('''CREATE TABLE IF NOT EXISTS ''' + nom_bdd + '''
    (id INTEGER PRIMARY KEY, nom TEXT, date DATETIME)''')

def AddEntry(nom_bdd, nom, date):
    QueryCurs.execute('''INSERT INTO ''' + nom_bdd + '''
    (nom,date) VALUES (?,?)''',(nom,date))

CreateTable("personne")


faceCascade = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')
video_capture = cv2.VideoCapture(0)
recognizer = cv2.face.LBPHFaceRecognizer_create()
recognizer.read("face-trainner.yml")

labels = {"person_name": 1}
with open("pickles/face-labels.pickle", 'rb') as f:
	og_labels = pickle.load(f)
	labels = {v:k for k,v in og_labels.items()}

while True :

    ret, frame = video_capture.read()
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    faces = faceCascade.detectMultiScale(
        gray,
        scaleFactor=1.3,
        minNeighbors=5
    )

    for(x,y,w,h) in faces:

        color = (255, 0, 0) #BGR 0-255 
        stroke = 2
        end_cord_x = x + w
        end_cord_y = y + h
        cv2.rectangle(frame, (x, y), (end_cord_x, end_cord_y), color, stroke)

        roi_gray = gray[y:y+h, x:x+w] #(ycord_start, ycord_end)
        roi_color = frame[y:y+h, x:x+w]

        id_, conf = recognizer.predict(roi_gray)
        if conf>=20 and conf <= 85:
    		#print(5: #id_)
            datetime_object = datetime.datetime.now()
            print(datetime_object)
            print(labels[id_])
            font = cv2.FONT_HERSHEY_SIMPLEX
            name = labels[id_]
            color = (255, 255, 255)
            stroke = 2
            cv2.putText(frame, name, (x,y), font, 1, color, stroke, cv2.LINE_AA)
            try:
                AddEntry('personne',labels[id_],datetime_object)
                CreateDataBase.commit()
            except:
                print("An exception occurred")
                
    
    cv2.imshow('Video', frame)

    if cv2.waitKey(1) & 0xFF == ord('q') :
        break

video_capture.release()
cv2.destroyAllWindows()