//L'application requiert l'utilisation du module Express.
var express = require('express'); 
var hostname = 'localhost'; 
var port = 3000; 
 
// Nous créons un objet de type Express. 
var app = express(); 
 
//Afin de faciliter le routage (les URL que nous souhaitons prendre en charge dans notre API), nous créons un objet Router.
//C'est à partir de cet objet myRouter, que nous allons implémenter les méthodes. 
var myRouter = express.Router(); 

const sqlite3 = require('sqlite3').verbose();
 
// open the database
let db = new sqlite3.Database('./DataBase.db', sqlite3.OPEN_READWRITE, (err) => {
  if (err) {
    console.error(err.message);
  }
  console.log('Connected to the database.');
});

 

 
// Nous demandons à l'application d'utiliser notre routeur
app.use(myRouter);  
 
// Démarrer le serveur 
app.listen(port, hostname, function(){
	console.log("Mon serveur fonctionne sur http://"+ hostname +":"+port); 
});
 

myRouter.use(function(request, response, next) {
      response.header("Access-Control-Allow-Origin", "*");
      response.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
      next();
    });


myRouter.route('/repas')
// GET
.get(function(req,res){ 
	  res.json({message : "Liste toutes les repas", methode : req.method, "repas" : [{"num" : 1, "regime" : "normal", "entree" : "salade", "plat" : "steack", "desert" : "yaourt aux fruits"}, {"num" : 2, "regime" : "sans sucre", "entree" : "salade", "plat" : "steack sans sauce", "desert" : "yaourt nature"}] });
})
//POST
.post(function(req,res){
      res.json({message : "Ajoute un nouveau repas à la liste", methode : req.method});
})
//PUT
.put(function(req,res){ 
      res.json({message : "Mise à jour des informations d'un repas dans la liste", methode : req.method});
})
//DELETE
.delete(function(req,res){ 
res.json({message : "Suppression d'un repas dans la liste", methode : req.method});  
}); 




myRouter.route('/animations')
// GET
.get(function(req,res){ 
	  res.json({message : "Liste toutes les animation", methode : req.method, "animation" : [{"num" : 1, "nom" : "bridge", "heure" : "15h", "lieu" : "Salle repos"}, {"num" : 2, "nom" : "Cinéma", "heure" : "18h", "lieu" : "Grande salle"}] });
})
//POST
.post(function(req,res){
      res.json({message : "Ajoute une nouvelle animation à la liste", methode : req.method});
})
//PUT
.put(function(req,res){ 
      res.json({message : "Mise à jour des informations d'une animation dans la liste", methode : req.method});
})
//DELETE
.delete(function(req,res){ 
res.json({message : "Suppression d'une animation dans la liste", methode : req.method});  
}); 

myRouter.route('/detection')
// GET
.get(function(req,res){ 
      var sql = "select * from personne ORDER BY id DESC LIMIT 1"
      var params = []
      db.all(sql, params, (err, rows) => {
          if (err) {
            res.status(400).json({"error":err.message});
            return;
          }
          res.json({
              "message":"Detection du visage",
              "data":rows
          })
        });
}); 

myRouter.route('/patients')
// J'implémente les méthodes GET, PUT, UPDATE et DELETE
// GET
.get(function(req,res){ 
	  res.json({message : "Liste toutes les patients", methode : req.method, "patient" : [{"num" : 1, "nom" : "Salze", "prenom" : "michael", "repas" : 1, "animations": [{"num" : 1},{"num" : 2}]}, {"num" : 2, "nom" : "Toto", "prenom" : "ginette", "repas" : 2, "animations": [{"num" : 2}]}] });
})
//POST
.post(function(req,res){
      res.json({message : "Ajoute un nouveau patient à la liste", methode : req.method});
})
//PUT
.put(function(req,res){ 
      res.json({message : "Mise à jour des informations d'un patient dans la liste", methode : req.method});
})
//DELETE
.delete(function(req,res){ 
res.json({message : "Suppression d'un patient dans la liste", methode : req.method});  
}); 